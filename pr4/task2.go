// task2
package main

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	err := ui.Main(func() {
		window := ui.NewWindow("Програма-калькулятор вартості туру на відпочинок", 550, 180, false)
		box := ui.NewVerticalBox()
		boxDaysQuantity := ui.NewHorizontalBox()
		boxCountry := ui.NewHorizontalBox()
		boxSeason := ui.NewHorizontalBox()
		boxQuide := ui.NewHorizontalBox()
		boxLux := ui.NewHorizontalBox()
		boxResult := ui.NewHorizontalBox()
		labelDaysQuantity := ui.NewLabel("Кількість днів:")
		inputDaysQuantity := ui.NewEntry()
		boxDaysQuantity.Append(labelDaysQuantity, true)
		boxDaysQuantity.Append(inputDaysQuantity, true)
		labelCountry := ui.NewLabel("Країна")
		country := ui.NewCombobox()
		country.Append("Болгарія")
		country.Append("Німеччина")
		country.Append("Польща")
		country.SetSelected(0)
		boxCountry.Append(labelCountry, true)
		boxCountry.Append(country, true)
		labelSeason := ui.NewLabel("Пора року")
		season := ui.NewCombobox()
		season.Append("Літо")
		season.Append("Зима")
		season.SetSelected(0)
		boxSeason.Append(labelSeason, true)
		boxSeason.Append(season, true)
		guide := ui.NewCheckbox("Індивідуальний гід")
		guide.SetChecked(false)
		boxQuide.Append(guide, true)
		luxe := ui.NewCheckbox("Номер люкс")
		luxe.SetChecked(false)
		boxLux.Append(luxe, true)
		labelResult := ui.NewLabel("")
		button := ui.NewButton("Розрахувати")
		boxResult.Append(labelResult, true)
		boxResult.Append(button, true)
		box.Append(boxDaysQuantity, false)
		box.Append(boxCountry, false)
		box.Append(boxSeason, false)
		box.Append(boxQuide, false)
		box.Append(boxLux, false)
		box.Append(boxResult, false)
		window.SetMargined(true)
		window.SetChild(box)
		button.OnClicked(func(*ui.Button) {
			days, err := strconv.ParseFloat(inputDaysQuantity.Text(), 2)
			result := 0.0
			if err == nil {
				if country.Selected() == 0 && season.Selected() == 0 {
					result = days * 100
				} else if country.Selected() == 0 && season.Selected() == 1 {
					result = days * 150
				} else if country.Selected() == 1 && season.Selected() == 0 {
					result = days * 160
				} else if country.Selected() == 1 && season.Selected() == 1 {
					result = days * 200
				} else if country.Selected() == 2 && season.Selected() == 0 {
					result = days * 120
				} else if country.Selected() == 2 && season.Selected() == 1 {
					result = days * 180
				}
				if luxe.Checked() == true {
					result *= 1.2
				}
				if guide.Checked() == true {
					result += days * 50
				}
				resultString := fmt.Sprintf("%.2f", result)
				resultString += " доларів."
				labelResult.SetText(resultString)
			}
		})
		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}

package main

import "fmt"

var x int = 5

func do(x interface{}) {
	switch x.(type) {
	case float64:
		fmt.Printf("1 ")
	case string:
		fmt.Printf("2 ")
	case int:
		fmt.Printf("3 ")
	default:
		fmt.Printf("4 ")
	}
}
func main() {
	const (
		_ = iota
		RED
		GREEN
		BLUE
		YELLOW = iota
		WHITE
	)
	print(_, RED, GREEN, BLUE, YELLOW, WHITE)
}

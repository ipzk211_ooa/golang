// worker
package main

import (
	"fmt"
	"strconv"
	"time"
)

type Company struct {
	Name     string
	Position string
	Salary   float32
}

func NewCompany(name string, position string, salary float32) *Company {
	return &Company{name, position, salary}
}

func (this *Company) SetName(new_name string) {
	this.Name = new_name
}

func (this *Company) SetPosition(new_pos string) {
	this.Position = new_pos
}

func (this *Company) SetSalary(new_sal float32) {
	this.Salary = new_sal
}

func (this Company) GetName() string {
	return this.Name
}

func (this Company) GetPosition() string {
	return this.Position
}

func (this Company) GetSalary() float32 {
	return this.Salary
}

type Worker struct {
	Name      string
	Year      int
	Month     int
	WorkPlace *Company
}

func NewWorker(name string, year int, month int, work_place *Company) Worker {
	tmp := Worker{}
	tmp.Name = name
	tmp.Year = year
	tmp.Month = month
	tmp.SetWorkPlaceLink(work_place)
	return tmp
}
func (this *Worker) SetName(new_name string) {
	this.Name = new_name
}
func (this *Worker) SetYear(new_year int) {
	this.Year = new_year
}
func (this *Worker) SetMonth(new_month int) {
	this.Month = new_month
}
func (this *Worker) SetWorkPlaceNew(name string, position string, salary float32) {
	tmp := NewCompany(name, position, salary)
	this.WorkPlace = tmp
}
func (this *Worker) SetWorkPlaceLink(work_place *Company) {
	this.WorkPlace = work_place
}
func (this Worker) GetName() string {
	return this.Name
}
func (this Worker) GetYear() int {
	return this.Year
}
func (this Worker) GetMonth() int {
	return this.Month
}
func (this Worker) GetWorkPosition() Company {
	return *this.WorkPlace
}
func (this *Worker) GetWorkExperience() int {
	that_date := time.Date(this.Year, time.Month(this.Month), 0, 0, 0, 0, 0, time.UTC)
	diff := time.Now().Sub(that_date)
	return int(diff.Hours() / 24 / 30)
}
func (this *Worker) GetTotalMoney() float32 {
	return float32(this.GetWorkExperience()) * this.GetWorkPosition().GetSalary()
}

func ReadWorkersArray() []Worker {
	var (
		n   int
		tmp string
		err error
	)

	for true {
		fmt.Print("Будь ласка, введіть кількість працівників: ")
		_, _ = fmt.Scan(&tmp)
		n, err = strconv.Atoi(tmp)
		if err != nil || n < 1 {
			fmt.Println("Введіть кількість працівників знову!")
		} else {
			break
		}
	}
	var workers []Worker
	for i := 0; i < n; i++ {
		var (
			w_name   string
			c_name   string
			position string
			year     int
			month    int
			salary   float32
		)
		fmt.Print("Введіть ім'я працівника: ")
		_, err := fmt.Scan(&w_name)
		for true {
			fmt.Print("Введіть рік початку роботи:")
			_, _ = fmt.Scan(&tmp)
			year, err = strconv.Atoi(tmp)
			if err != nil || year > 2022 || year < 0 {
				fmt.Println("Введіть рік знову!")
			} else {
				break
			}
		}

		for true {
			fmt.Print("Введіть місяць початку роботи: ")
			_, _ = fmt.Scan(&tmp)
			month, err = strconv.Atoi(tmp)
			if err != nil || month < 1 || month > 12 {
				fmt.Println("Введіть місяць знову!")
			} else {
				break
			}
		}
		fmt.Print("Введіть назву компанії: ")
		_, _ = fmt.Scan(&c_name)
		fmt.Print("Введіть посаду, на якій він працює:")
		_, _ = fmt.Scan(&position)

		for true {
			fmt.Print("Введіть його зарплату: ")
			_, _ = fmt.Scan(&tmp)
			var salary_tmp float64
			salary_tmp, err = strconv.ParseFloat(tmp, 32)
			if err != nil || salary < 0 {
				fmt.Println("Введіть його зарплату знову!")
			} else {
				salary = float32(salary_tmp)
				break
			}
		}
		if err == nil {
			fmt.Println("Працівник був успішно доданий.")
			new_company := NewCompany(c_name, position, salary)
			new_worker := NewWorker(w_name, year, month, new_company)
			workers = append(workers, new_worker)
		}
	}
	return workers
}

func PrintWorker(w Worker) {
	fmt.Printf("%-10s %-6d %-15d %-20.2f %-10s %-15s %-5d\n", w.Name, w.Year, w.Month, w.WorkPlace.Salary, w.WorkPlace.Position, w.WorkPlace.Name, w.GetWorkExperience())
}
func PrintWorkers(ws []Worker) {
	fmt.Println("Працівники:")
	for i := 0; i < len(ws); i++ {
		PrintWorker(ws[i])
	}
	fmt.Println("****")
}
func GetWorkersInfo(ws []Worker) (min Worker, max Worker) {
	min = ws[0]
	max = ws[0]
	for i := 0; i < len(ws); i++ {
		if ws[i].WorkPlace.Salary < min.GetWorkPosition().GetSalary() {
			min = ws[i]
		}
		if ws[i].WorkPlace.Salary > max.GetWorkPosition().GetSalary() {
			max = ws[i]
		}
	}
	return min, max
}
func main() {
	ws := ReadWorkersArray()
	fmt.Printf("%-10s %-6s %-15s %-20s %-10s %-15s %-5s\n", "Ім'я", "Рік", "Місяць", "Зарплата", "Позиція", "Компанія", "Досвід(місяці)")
	PrintWorkers(ws)
	min, max := GetWorkersInfo(ws)
	PrintWorker(min)
	PrintWorker(max)
}

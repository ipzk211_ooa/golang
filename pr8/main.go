package main

import (
	// "fmt"
	"html/template"
	"net/http"
	// "strconv"
)

type Variables struct {
	Title        string
	HeaderThree  string
	ParagraphOne string
	ParagraphTwo string
	Image        string
}

const (
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	myVars := Variables{}
	tmplFile := "main.tpl"
	myVars.Title = "Home"

	if r.URL.Path == "/cities" {
		tmplFile = "cities.tpl"
		myVars.Title = "Cities"
	}

	if r.URL.Path == "/cities/1" {
		tmplFile = "city.tpl"
		myVars.Title = "Cities"
		myVars.HeaderThree = "Dubai, United Arab Emirates"
		myVars.ParagraphOne = "Dubai is riding the high of Expo 2020 (which technically took place 2021–2022), a multibillion-dollar, six-month world’s fair showcasing nearly 200 nations with future-forward pavilions and seemingly round-the-clock cultural celebrations. Now the UAE hub wants to inspire loyalty (and repeat trips) among its visitors: Dubai is aiming to be the world’s most-visited tourist destination, targeting 25 million visitors by 2025. A new United direct flight between Newark and Dubai launches March 2023—that should help."
		myVars.ParagraphTwo = "Why we love it: its new Museum of the Future; greater accessibility with more affordable hotel options; a new Michelin guide featuring 69 restaurants, including a Green star sustainability honor; and the lure of Santiago Calatrava’s Tower at Dubai Creek, expected to eclipse the Burj Khalifa as the tallest building in the world."
		myVars.Image = "https://afar.brightspotcdn.com/dims4/default/c5bb588/2147483647/strip/true/crop/1300x867+0+0/resize/1440x960!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F14%2Fa6%2F6055e596497f55ef2e1c07e734ce%2Foriginal-dubai-shutterstock-1204872664.jpg"
	}

	if r.URL.Path == "/cities/2" {
		tmplFile = "city.tpl"
		myVars.Title = "Cities"
		myVars.HeaderThree = "London, England"
		myVars.ParagraphOne = "Despite a year when England’s longest-reigning monarch passed away … despite three prime ministers in mere months … “Despite much-warranted hand-wringing about the flight of talent and capital due to the pall of Brexit (and the follow-up specter of an airborne pandemic), London is hanging in just fine,” says Resonance, “relying on a dipping currency to attract investment and, of course, previously priced-out tourists. And new residents. New wealthy residents who can now afford to check off a big item on the multimillionaire bucket list: property in the best city on the planet. … According to fDi Markets, the Financial Times’ foreign investment tracker, London has pulled in the most foreign direct investments into tech from international companies since 2018, ahead of New York, Singapore, and Dubai.”"
		myVars.ParagraphTwo = "Why we love it: London’s main sights might date back millennia, but the capital’s shops, bars, hotels, and restaurants continue to evolve on an almost weekly basis. Whether you’re outdoorsy, hungry, or bringing a family in tow, there’s a distinct London neighborhood to investigate—and it will likely look different from your last visit. For new hotel options, there’s everything from the much-hyped NoMad London to hip One Hundred Shoreditch to revitalized historic classics such as the Dilly."
		myVars.Image = "https://afar.brightspotcdn.com/dims4/default/fbc3734/2147483647/strip/true/crop/1300x867+0+0/resize/1440x960!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fb2%2Fac%2F9f4008acfaf6ef0e1497dabbefed%2Foriginal-london-shutterstock-240016375.jpg"
	}

	if r.URL.Path == "/cities/3" {
		tmplFile = "city.tpl"
		myVars.Title = "Cities"
		myVars.HeaderThree = "New York City, United States"
		myVars.ParagraphOne = "NYC is welcoming back visitors in style with major upgrades to its international gateways, says Resonance. LaGuardia Airport, Newark Liberty International Airport, and John F. Kennedy International Airport all have new terminals, “with the new Terminal B at LaGuardia alone boasting 35 gates” and looking fine. “Back on the ground, Moynihan Train Hall is a new 17-track expansion of Penn Station that, if you squint, can pass for a northern European transit hub from the future.” And if you haven’t heard of the biggest hotel opening in the city this summer, just check out AFAR’s review of the new $3,200-a-night Aman."
		myVars.ParagraphTwo = "Why we love it: “The City” consistently ranks for its culture; new this year is a Museum of Broadway, an expanded Louis Armstrong House Museum, the Bronx Children’s Museum, a jazz club with Lincoln Center acoustics at Aman. New to the Sights and Landmarks list: a slew of seriously legit food halls like the Singaporean/Malaysian Urban Hawker Center and the Tin Building by Jean-Georges at revamped South Street Seaport. And as Broadway comes back with a full roster, we expect visitors will follow."
		myVars.Image = "https://afar.brightspotcdn.com/dims4/default/4b64734/2147483647/strip/true/crop/1300x757+0+0/resize/1440x839!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fe3%2Fc5%2F9b048222d41aaeddde85fc0a9f8d%2Foriginal-nyc-colton-duke-uexx0knnkjy-unsplash.jpg"
	}
	tmpl, err := template.ParseFiles(tmplFile)
	templates := template.Must(tmpl, err)

	templates.ExecuteTemplate(w, tmplFile, myVars)
}
func main() {
	http.ListenAndServe(":8080", HttpSolution1)
}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ .Title }}</title>
</head>
<body>
<style>
    body {
        font-family: "Lato", sans-serif;
        font-size: 20px;
        background: #DEDCDD;
    }

    .tablink {
        background-color: #449192;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 20%;
    }

    .tablink:hover {
        background-color: #F7A5A0;
    }

    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial;
    }

    .header {
        text-align: center;
        padding: 32px;
    }

    .row {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        -ms-flex-wrap: wrap; /* IE10 */
        flex-wrap: wrap;
        padding: 0 4px;
    }

    .column {
        -ms-flex: 25%; /* IE10 */
        flex: 25%;
        max-width: 25%;
        padding: 0 4px;
    }

    .column img {
        margin-top: 8px;
        vertical-align: middle;
        width: 100%;
    }

</style>

    <a class="tablink" href="/">Article</a>
    <a class="tablink" href="/cities">Cities</a>
    <a class="tablink" href="/cities/1">City №1</a>
    <a class="tablink" href="/cities/2">City №2</a>
    <a class="tablink" href="/cities/3">City №3</a>

<div class="row">
    <div class="column">
        <img src="https://afar.brightspotcdn.com/dims4/default/851a4a3/2147483647/strip/true/crop/1300x975+0+0/resize/1440x1080!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F2d%2F3c%2Fae6b27b619cfdfc5b0da0ad6a508%2Foriginal-rome-christopher-czermak-pamffhl6fvy-unsplash.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/f23353d/2147483647/strip/true/crop/1300x888+0+0/resize/1440x984!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F9e%2F88%2Fe23037733b90bf234e30469921bc%2Foriginal-barcelona-luis-pina-shutterstock-580710487-copy.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/128420e/2147483647/strip/true/crop/1300x840+0+0/resize/1440x930!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fe1%2Fff%2Fe27ebc8f22e766b2a37b4f827d23%2Foriginal-tokyo-shutterstock-1039699957.jpg" style="width:100%">
    </div>
    <div class="column">
        <img src="https://afar.brightspotcdn.com/dims4/default/8ebd310/2147483647/strip/true/crop/1600x1081+0+0/resize/1440x973!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F6b%2F03%2F5bdbb568f0a761e6150e3b270b3d%2Foriginal-lede-madrid-yulia-grigoryeva-shutterstock-186198893.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/82db6ad/2147483647/strip/true/crop/1300x867+0+0/resize/1440x960!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F11%2F0e%2F46cf1fe7b700a52388767f948e73%2Foriginal-singapore-shutterstock-125810687.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/8ef4251/2147483647/strip/true/crop/1300x867+0+0/resize/1440x960!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F0b%2F06%2F32688147d2727e27a527089a7e5e%2Foriginal-amsterdam-shutterstock-559798888.jpg" style="width:100%">
    </div>
    <div class="column">
        <img src="https://afar.brightspotcdn.com/dims4/default/8ab4f64/2147483647/strip/true/crop/1300x868+0+0/resize/1440x961!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fb1%2F68%2Fed509c216d2176cb6de726ee59b2%2Foriginal-paris-catarina-belova-shutterstock-1029106939.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/ce6868c/2147483647/strip/true/crop/1000x667+0+0/resize/1440x960!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F98%2Ffb%2Fce1820d747e59ef641e6cb75d8a9%2Fepictrips-eurotrain-05-graz.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/b85f54f/2147483647/strip/true/crop/1900x792+0+263/resize/1440x600!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F1b%2F29%2F424c0a4ca923518fa62fe17e66e2%2Foriginal-london-shutterstock-92655643.jpg" style="width:100%">
    </div>
    <div class="column">
        <img src="https://afar.brightspotcdn.com/dims4/default/6f235ef/2147483647/strip/true/crop/1440x720+0+0/resize/1440x720!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fbe%2F52%2F4baead194444bed4809137eeefa3%2Fafr090122-gaziantep-01.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/9418145/2147483647/strip/true/crop/1000x521+0+17/resize/840x438!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2Fa6%2Fca%2F5e32406c4c40a4065b3e4ac32e93%2Fshutterstock-501976372.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/55dc07a/2147483647/strip/true/crop/1920x1001+0+139/resize/840x438!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F45%2F4c%2Feb424dbc41dba218856e1e4d6d80%2Fdan-freeman-7zb7kuyqg1e-unsplash.jpg" style="width:100%">
        <img src="https://afar.brightspotcdn.com/dims4/default/d19f193/2147483647/strip/true/crop/1000x521+0+20/resize/840x438!/format/webp/quality/90/?url=https%3A%2F%2Fafar-media-production-web.s3.amazonaws.com%2Fbrightspot%2F54%2F0a%2Ff32a965647dcbbccac23efbe47ce%2Fshutterstock-1601057362.jpg" style="width:100%">
    
    </div>
</div>

</body>
</html>
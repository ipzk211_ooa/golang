<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ .Title }}</title>
</head>
<body>
<style>
    body {
        font-family: "Lato", sans-serif;
        font-size: 20px;
        background: #DEDCDD;
    }

    .tablink {
        background-color: #449192;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 20%;
    }

    .tablink:hover {
        background-color: #F7A5A0;
    }

    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial;
    }

    .header {
        text-align: center;
        padding: 32px;
    }

    .row {
        display: -ms-flexbox; 
        display: flex;
        -ms-flex-wrap: wrap; 
        flex-wrap: wrap;
        padding: 0 4px;
    }
    
    .column {
        -ms-flex: 50%; 
        flex: 50%;
        max-width: 50%;
        padding: 0 4px;
    }

    .column img {
        margin-top: 8px;
        vertical-align: middle;
        width: 100%;
    }

    h3 {
        text-align:center;
    }
    p{
        text-indent: 20px;
        max-width: 1000px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 50px;
    }
</style>

    <a class="tablink" href="/">Article</a>
    <a class="tablink" href="/cities">Cities</a>
    <a class="tablink" href="/cities/1">City №1</a>
    <a class="tablink" href="/cities/2">City №2</a>
    <a class="tablink" href="/cities/3">City №3</a>

<div class="row">
    <div class="column">
        <h3>{{ .HeaderThree }}</h3>
        <p>{{ .ParagraphOne }}</p>
        <p>{{ .ParagraphTwo }}</p>
    </div>
    <div class="column">
        <img src="{{ .Image }}"
             style="width:100%">
        
    </div>
</div>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ .Title }}</title>
</head>
<body>
<style>
    body {
        font-family: "Lato", sans-serif;
        font-size: 20px;
        background: #DEDCDD;
    }

    .tablink {
        background-color: #449192;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 20%;
    }

    .tablink:hover {
        background-color: #F7A5A0;
    }

    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial;
    }

    .header {
        text-align: center;
        padding: 32px;
    }
    p {
        text-indent: 20px;
        max-width: 1000px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 50px;
    }
    
    .container {
        margin-top: -30px;
    }
</style>

<div class="container" style="background: red">
    <a class="tablink" href="/">Article</a>
    <a class="tablink" href="/cities">Cities</a>
    <a class="tablink" href="/cities/1">City №1</a>
    <a class="tablink" href="/cities/2">City №2</a>
    <a class="tablink" href="/cities/3">City №3</a>
</div>

<div class="row"></div>
<h1 style="text-align:center;">The World’s Best Cities of 2023</h1>

<p style="font-style: italic;">
    Unlike other best-of lists, Resonance’s annual ranking considers a city’s diversity, cultural programming, safety, number of parks, even sunny days.
</p>
<p>
    Someday, we will start a story without referencing the “pandemic pause”—but this is not that day. Things are still in flux for the world’s best cities. In the past year, the global population started moving again en masse, in pursuit of the ideal place to work, live, and play. Hundreds of thousands left major metropolitan centers like New York, San Francisco, and Rome for more affordable pastures; meanwhile, LinkedIn reports that one in six job listings is “remote,” fueling somewhat of a planet-wide existential crisis. Does this spell the end for the big city?
</p>
<p>
    “Far from it,” said Chris Fair, president and CEO of Resonance Consultancy, whose 2023 World’s Best Cities list was released today with many familiar destinations at the top.
</p>
<p>
    For the past 15 years, Resonance—a consultancy group in real estate, tourism, and economic development—has taken a holistic approach to a popular “best-of” list. Rather than just rely on data around, say, a place’s livability or how easy it is to bike there, Resonance uses a combination of core statistics (like GDP and homicide rates) and qualitative evaluations by both locals and visitors (from online channels like Instagram and TripAdvisor) to paint a more comprehensive picture of a world’s best city. “It’s not just best city to live, it’s not just best city to work, or best city to visit,” said Fair. “It’s taking a cross section of all those factors.”
</p>
<p>
    The cities on this year’s list—all with populations of more than 750,000—have used the “pandemic pause” to roll out 464 miles of bikeways (San Francisco), spend billions on hotels and waterfront development (Washington, D.C.), expand or open major museums and finally finish that LaGuardia airport renovation (NYC). One destination even changed its name—remember, it’s now Istanbul, Türkiye. And common across so many of these top-tier cities is a commitment to diversity and sustainability: no longer “forward thinking,” now just the norm of the biggest thinkers.
</p>
<h2 style="text-align:center;">
    How the best city rankings work
</h2>
<p>
    “Many of the factors that people told us were important in choosing a city to live or do business or visit were related to the experiential quality of the city—things like culture, restaurants, nightlife, shopping, and sports,” Fair said. “There are no core statistics for those kinds of factors. What really distinguishes our rankings is that we are mining user-generated data in channels like TripAdvisor and Yelp to measure those experiential factors.”
</p>
<p>
    Those areas they ranked cities on were grouped into six core categories, including Place, People, Programming, Product, Prosperity, and Promotion.
</p>
<p>
    <strong>Place:</strong> This includes weather (the average number of sunny days), safety (homicide rate), as well as sights and landmarks (specifically the number of which were recommended by locals and visitors) and outdoors (or the number of parks and outdoor activities recommended by locals and visitors).
</p>
<p>
    <strong>People:</strong> The People category considers educational attainment (percentage of population with a bachelor’s degree or higher) and percent of citizens participating in the labor force.
</p>
<p>
    <strong>Programming:</strong> This is what most guidebooks would call “things to do” and includes experiences offered in the areas of culture (specifically performing arts), nightlife, dining, and shopping recommended by both locals and visitors.
</p>
<p>
    <strong>Product:</strong> The Product category, on the other hand, includes each city’s infrastructure and institutions. This is where attractions and museums are considered, as well as other areas like airport connectivity (or the number of direct destinations served by the city’s airports), university ranking (specifically the ranking of the top local school), and the size of the local convention center.
</p>
<p>
    <strong>Prosperity:</strong> This category includes the number of Global 500 corporate headquarters located within each city, the GDP per capita, the income equality index, and the unemployment rate. While most travelers wouldn’t necessarily factor these things into choosing a destination, Resonance believes greater “prosperity” draws more people to live in these cities, which eventually drives more economic growth and development. That means better dining options, cultural institutions, and airports in the long run.
</p>
<p>
    <strong>Promotion:</strong> In addition to relying on user-generated data from locals and visitors to vet dining and shopping recommendations, this list also looked at how popular each city was online. The Promotion category—or how a city’s story is shared through online channels—is based specifically on the number of Facebook check-ins, Google searches, TripAdvisor reviews, and Instagram hashtags shared online about each city, as well as the popularity of each city in Google Trends over the last 12 months.
</p>
</body>
</html>
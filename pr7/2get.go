package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
)

const (
	pageHeader = `<!DOCTYPE HTML>
<html>
<head>
<title>Task2-get</title>
<style>
.error{
color:#FE2712;
}
</style>
</head>`
	pageBody = `<body>
<h1>Одновимірний зріз</h1>`
	form = `<form action="/" method="GET">
<label for="a">Введіть кількість чисел:</label><br />
<input type="number" name="a" step="1" value="1" min="1"><br />

<input type="submit" value="Згенерувати зріз">
</form>`
	pageFooter = `</body></html>`
	anError    = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Завдання2"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form)
	if r.Method == "GET" {
		query := r.URL.Query()
		a, err := strconv.ParseInt(query.Get("a"), 10, 64)
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		min := -100.0
		max := 100.0
		array := make([]float64, a)
		for i := range array {
			array[i] = min + rand.Float64()*(max-min)
		}
		fmt.Fprintf(w, "%.2f", array)
		oddSum := 0.0
		multiplication := 1.0
		for i := range array {
			if i%2 != 0 {
				oddSum += array[i]
			}
		}

		flag := 0
		var flagIndexArr []int

		for i := range array {
			if array[i] == 0 {
				flag += flag
				flagIndexArr = append(flagIndexArr, i)
			}
		}

		if flag == 2 {
			for i := flagIndexArr[0]; i <= flagIndexArr[1]; i++ {
				multiplication *= array[i]
			}
		}

		if multiplication == 1 {
			multiplication = 0
		}

		fmt.Fprintf(w, "<br><br>Сума елементів з непарними номерами: %.2f<br> Добуток елементів, розташованих між першим і останнім нульовими елементами: %.2f", oddSum, multiplication)
	}
	fmt.Fprint(w, "\n", pageFooter)
}

func main() {
	http.ListenAndServe(":8080", HttpSolution1)
}

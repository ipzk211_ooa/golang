package main

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)

const (
	pageHeader = `<!DOCTYPE HTML>
<html>
<head>
<title>Task1-post</title>
<style>
.error{
color:#FE2712;
}
</style>
</head>`
	pageBody = `<body>
<h1>Вирішення квадратичного рівняння</h1>`
	form = `<form action="/" method="POST">
	<label>Квадратичне рівняння вигляду ax^2 + bx + c = 0</label><br />
<label for="a">Введіть змінну a:</label><br />
<input type="number" name="a" step="0.01" value="0"><br />
<label for="b">Введіть змінну b:</label><br />
<input type="number" name="b" step="0.01" value="0"><br />
<label for="c">Введіть змінну c:</label><br />
<input type="number" name="c" step="0.01" value="0"><br />

<input type="submit" value="Вирішити">
</form>`
	pageFooter = `</body></html>`
	anError    = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Завдання1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form)
	if r.Method == "POST" {
		err := r.ParseForm()
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		a, err := strconv.ParseFloat(post.Get("a"), 64)
		b, err := strconv.ParseFloat(post.Get("b"), 64)
		c, err := strconv.ParseFloat(post.Get("c"), 64)
		fmt.Fprintf(w, "Коефіцієнти:<br>a = %v<br> b = %v<br>c = %v", a, b, c)

		x1 := (-b + math.Sqrt(b*b-4*a*c)) / (2 * a)
		x2 := (-b - math.Sqrt(b*b-4*a*c)) / (2 * a)
		fmt.Fprintf(w, "<br><br>Розв'язок:<br>x1 = %.2f<br> x2 = %.2f", x1, x2)
	}
	fmt.Fprint(w, "\n", pageFooter)
}
func main() {
	http.ListenAndServe(":8080", HttpSolution1)
}

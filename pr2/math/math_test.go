package math

import (
	"testing"
)

func TestMin(t *testing.T) {
	x := Min(-2, 2, 4)
	res := -2.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestSer(t *testing.T) {
	x := Ser(-5, 0, 11)
	res := 2.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestEquation(t *testing.T) {
	x := Equation(-2, 10)
	res := -5.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

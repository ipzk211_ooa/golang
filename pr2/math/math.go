package math

func Min(a float64, b float64, c float64) float64 {
	if a < b && a < c {
		return a
	} else if b < a && b < c {
		return b
	} else {
		return c
	}
}

func Ser(a float64, b float64, c float64) float64 {
	return (a + b + c) / 3
}

func Equation(a float64, b float64) float64 {
	return b / a
}

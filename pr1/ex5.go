package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Синонимы целых типов\n")

	fmt.Println("byte    - int8")
	fmt.Println("rune    - int32")
	fmt.Println("int     - int32, или int64, в зависимости от ОС")
	fmt.Println("uint    - uint32, или uint64, в зависимости от ОС")

	var val int = math.MaxInt
	var x string
	if val == 9223372036854775807 {
		x = "x64"
	} else {
		x = "x32"
	}
	fmt.Println("Розрядність -", x)
}

package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	var letter int16 = 'Ї'
	fmt.Printf("Українська буква '%c' - %d\n", letter, letter)
}

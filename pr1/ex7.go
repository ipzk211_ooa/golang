package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	var num1 int = 10
	var num2 float64 = 3.2795
	fmt.Printf("(-) | %d - %.2f = %f\n", num1, num2, float64(num1)-num2)
	fmt.Printf("(+) | %d + %.2f = %f\n", num1, num2, float32(num1)+float32(num2))
	fmt.Printf("(-) | %d %% %.2f = %d\n", num1, num2, num1%int(num2))
}

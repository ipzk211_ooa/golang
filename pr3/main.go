package main

import (
	"fmt"
	"lab3/myRandom"
	"math"
)

func main() {
	var arr = myRandom.GenerateRandomIntArray(300, 50000)
	for i := 0; i < len(arr); i++ {
		fmt.Printf("%d\n", arr[i])
	}
	var arr2 = myRandom.GenerateRandomFloatArray(300, 50000)
	for i := 0; i < len(arr2); i++ {
		fmt.Printf("%.2f\n", arr2[i])
	}

	interval := myRandom.Interval(arr)
	frequency := myRandom.Frequency(interval)

	expectation := myRandom.Expectation(frequency)
	dispersion := myRandom.Dispersion(frequency, expectation)

	fmt.Printf("\nСередньоквадратичне відхилення: %.2f \n", math.Sqrt(dispersion))
}

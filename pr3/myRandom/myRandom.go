package myRandom

import (
	"fmt"
	"math"
	"time"
)

const (
	a uint64 = 1103515245
	c uint64 = 12345
	m uint64 = 1 << 31
)

var seed = uint64(time.Now().Unix())

func GenerateRandomIntArray(max uint64, iCount int) []uint64 {
	var nextNumber = (a*seed + c) % m
	var array []uint64

	for i := 0; i < iCount; i++ {
		nextNumber = (a*nextNumber + c) % m % max
		array = append(array, nextNumber)
	}

	return array
}

func GenerateRandomFloatArray(max uint64, iCount int) []float64 {
	var nextNumber = (a*seed + c) % m
	var array []float64

	for i := 0; i < iCount; i++ {
		nextNumber = (a*nextNumber + c) % m % (max * 100)
		array = append(array, float64(nextNumber)/100)
	}

	return array
}

func Interval(array []uint64) map[uint64]uint64 {
	dictionary := make(map[uint64]uint64)

	for _, num := range array {
		dictionary[num] = dictionary[num] + 1
	}

	for num, count := range dictionary {
		fmt.Printf("Число \"%d\" повторюється %d разів \n", num, count)
	}

	return dictionary
}

func Frequency(dictionary map[uint64]uint64) map[uint64]float64 {
	frequency := make(map[uint64]float64)

	for index, val := range dictionary {
		num := float64(val) / float64(50000)
		frequency[index] = num
		fmt.Printf("Статична ймовірність числа \"%d\": %.2f \n", index, num)
	}

	return frequency
}

func Expectation(frequency map[uint64]float64) float64 {
	expectation := 0.0

	for val, freq := range frequency {
		expectation += float64(val) * freq
	}
	fmt.Printf("\nМатематичного сподівання випадкових величин: %.2f\n", expectation)

	return expectation
}

func Dispersion(frequency map[uint64]float64, exp float64) float64 {
	dispersion := 0.0

	for val, freq := range frequency {
		dispersion += math.Pow(float64(val)-exp, 2) * freq
	}
	fmt.Printf("\nДисперсії випадкових величин: %.2f \n", dispersion)

	return dispersion
}
